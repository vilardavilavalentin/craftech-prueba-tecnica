# Resolucion de ejercicios para prueba tecnica de Craftech



## Ejercicio 1

![image](eks-diagram.jpeg)

Se necesita desplegar una aplicacion que tiene los siguientes requerimientos:
- Cargas variables
- Contar con HA (alta disponibilidad)
- Frontend en Js
- Backend con una base de datos relacional y una no relacional
- La aplicación backend consume 2 microservicios externos

Mi propuesta es un cluster de kubernetes en AWS utilizando el Servicio EKS.
Mi idea es hacer algo simple y estandar , sin complicaciones pero sin dejar de lado aspectos como la seguridad, escalabilidad y robustez.
Este cluster cuenta con 3 Nodos (cualquier instance type) desplegados en 3 Availabilty zones distintas para tener alta disponibilidad.
Estos nodos son parte de un ManagedNodeGroup lo que les permite escalar y desescalar segun las reglas que uno establezca.
Ademas estan desplegados en una subnet privada para que no tengan salida directa a internet y asi ser mas seguros.
Como base de datos SQL se utiliza Amazon RDS y como NoSQL se utiliza Amazon DynamoDB. Estas bases de datos estan en una vpc distinta para un mayor aislamiento y se conectan con la vpc del cluster a traves de vpc peering.
Se asume que los microservicios se consumen directamente de internet.


Ademas me gustaria agregar un poco de el software que se podria utilizar adentro de este cluster:

- para monitorizacion: Utilizaria prometheus junto con Grafana y Ademas amazon cloudwatch
- para centralizacion de logs: Utilizaria Loki y amazon cloudwatch
- para realizar backups: Velero
- para manejo de secrets: Kubeseal
- Adicionalmente agregaria HPA y VPA.


## Ejercicio 2

### docker-compose

Para levantar el docker-compose en una pc, ejecutar el comando "docker-compose up"
esto buildeara las imagenes de los servicios de frontend y backend desde sus respectivas carpetas utilizando los Dockerfile.

Los Dockerfile se encuentran comentados linea por linea para explicar lo que se hace.

### algunos obstaculos superados durante el proceso

backend

- Se decidio utilizar python:3-buster ya que con python:3-buster-slim ocurrian errores con algunas dependencias
- hubo un error con la libreria psycopg2, se soluciono subiendo la version a la 2.9.2
- hubo un error al no estar seteada la variable SECRET_KEY, por lo que se seteo una dummy para que buildeara exitosamente
- hubo un error al instalar la libreria pygraphviz, se soluciono instalando otras dependencias en la imagen
- adicionalmente se agrego un dockerignore para evitar copiar ciertos archivos como la carpeta pycache

frontend

- no ocurrieron obstaculos significativos
- como observacion la app parece estar vacia, problemente se deba a que falta una base de datos

### Como desplegar la app en la nube 

Para desplegar la aplicacion en la nube hay varias opciones, pero hay que tener en cuenta que un stack de estas caracteristicas deberia utilizarse solo en casos de desarollo ya que no se esta teniendo en cuenta cosas como la escalabilidad, la seguridad, entre otras.

algunas de las opciones utilizando docker-compose son:

- Se podria crear un cluster de docker swarm y utilizar el docker-compose para levantar los servicios.

- Se podria deployar la aplicacion directamente en una maquina virtual (Utilizando por ejemplo EC2) y en este caso tambien se podria utilizar el docker-compose.


otra opcion seria solamente utilizar las imagenes de docker y en este caso se podria:

- desplegar las imagenes en un managed K8S cluster (como por ejemplo EKS) y hacer que se comuniquen a traves de un servicio



habria que evaluar los requisitos del cliente y el presupuesto para elegir la opcion mas adecuada.


## Ejercicio 3

Se realizo una pipeline utilizando Gitlab CI, que se triggerea con la modificacion del archivo index.html
y realiza un build a la imagen de NGINX con el archivo modificado para luego deployarlo en un cluster de Kubernetes de GKE

El dominio para ver el cambio implementado es

https://k8s.valenvilardav.xyz
